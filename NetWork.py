import numpy as np

class Network(object):

	def __init__(self):
		self.w1 = np.random.sample((2, 2)) 
		self.b1 = np.random.sample(2)
		self.w2 = np.random.sample((1,2))
		self.b2 = np.random.sample(1)
		self.w = [self.w1, self.w2] #Веса
		self.b = [self.b1, self.b2] #Биасы
		self.tri_W = {} #Словарь для дельт весов
		self.tri_b = {} #Словарь для дельт биасов
		nn_structure = [2,2,1] #Кол-во нейронов в каждом слое
		for l in range(1, len(nn_structure)): #Заполняем матрицы дель весов нулями
			self.tri_W[l] = np.zeros((nn_structure[l], nn_structure[l-1]))
			self.tri_b[l] = np.zeros((nn_structure[l],))

	#Функция сигмоиды
	def Sigmoid(self,z):
		return 1.0/ (1.0 + np.exp(-z) )

	#Функция производной сигмоиды
	def SigmoidDerivative(self,z):
		s = self.Sigmoid(z)
		return s*(1.0-s)

	#Процесс прямого распространения
	def Forward(self, inputs):
		#Вход и выход скрытого слоя
		in_hiden_lay = self.w[0].dot(inputs.T)+self.b[0]
		out_hiden_lay = self.Sigmoid(in_hiden_lay)
		#Вход и выход выходного слоя
		in_out_lay = self.w[1].dot(out_hiden_lay) + self.b[1]
		out_out_lay = self.Sigmoid(in_out_lay)
		#Сохраняем данные выхода слоёв
		output = [out_hiden_lay, out_out_lay]
		return output

	#Обратное распространение ошибки
	def Backward(self, output, target):
		#Дельта для внешнего слоя
		e2 = - (target - output[1])
		d2 = e2*self.SigmoidDerivative(output[1])
		m = np.asmatrix(output[1])
		self.tri_W[2] += d2.dot(m.T)
		self.tri_b[2] += d2	
		#дельта для скрытого слоя	
		e1 = self.w[1].T.dot(d2)
		d1 = e1*self.SigmoidDerivative(output[0])
		m = np.asmatrix(output[0])
		self.tri_W[1] += d1.dot(m.T)
		self.tri_b[1] += d1

		alpha = 0.1
		m = np.asmatrix(output[0])
		#Пересчитываем веса 
		self.w[1] += - alpha * (1/2*self.tri_W[2])
		self.b[1] += - alpha * (1/2*self.tri_b[2])
		self.w[0] += - alpha * (1/2*self.tri_W[1])
		self.b[0] += - alpha * (1/2*self.tri_b[1])
