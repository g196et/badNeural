import random, math
import pandas as pd
import numpy as np
import NetWork as nw

df = pd.read_csv('data.csv',header=0)

Y = np.array(df[["label"]])
X = np.array(df[["grade1","grade2"]])

PerNet = nw.Network()

#Тестирование сети
def TestNetwork(Net,X,Y):
	score = 0 #Переменная для подсчёта результата
	for j in range(len(Y)):
		inputs = X[j]
		target = Y[j]
		#Предсказываем
		prediction = Net.Forward(inputs)
		tmp = prediction[1][0]
		#округляем предсказания для сравнения с данными
		rounded_prediction = [round(tmp)]
		print ('---')
		print ('prediction: ', rounded_prediction)
		print ('target    : ', target)
		#Если предсказание совпало с данными прибавляем к результату 1
		if rounded_prediction == target:
			score += 1

	print ('score is :', float(score) / 30.0)
	print ('score is :', score)

# Тренировка сети (Сеть, кол-во итераций, Входные данные, Выходные данные, Размер тренировочной выборки)
def TrainNetwork(Net,iterations,X,Y,TrainSize):
	#Делим данные на тренировочные и тестовые
	X_train = X[:TrainSize]
	Y_train = Y[:TrainSize]	

	X_test = X[TrainSize:]
	Y_test = Y[TrainSize:]

	print ("Начало тренировки")
	for x in range(iterations):
		#Выбираем рандомную строку данных
		i = random.randint(0,len(Y_train)-1)
		inputs = X_train[i]
		target = Y_train[i]
		#Проходим вперёд по сети
		output = Net.Forward(inputs)
		#Проходим обратно, исправляя ошибки
		Net.Backward(output, target)
	print ("Конец тренировки!")
	#Запускаем тестирование сети
	TestNetwork(Net,X_test,Y_test)


TrainNetwork(PerNet,20000,X,Y,70)

